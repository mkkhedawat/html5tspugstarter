const gulp = require('gulp');
const chalk = require('chalk');
const ts = require('gulp-typescript');
const tsProject = ts.createProject('tsconfig.json');
const pug = require('gulp-pug');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();
const postcss = require('gulp-postcss');
const sourcemaps = require('gulp-sourcemaps');
const vandana = require('vandana');
const precss = require('precss');
const prefixer = require('autoprefixer');

gulp.task('browser-sync', () => {
    browserSync.init({
        server: {
            baseDir: './',
            index: 'index.html'
        }
    });
    gulp.watch('./scss/*.scss', ['postcss', 'reload']);
    gulp.watch('./*.pug', ['pug', 'reload']);
    gulp.watch('./ts/*.ts', ['ts', 'reload']);
});

gulp.task('reload', () => {
    browserSync.reload();
});

gulp.task('ts', () => {
    log('Running typescript task ', 'green');
    return tsProject
        .src()
        .pipe(tsProject())
        .js
        .pipe(gulp.dest('js'));
});

gulp.task('pug', () => {
    log('Running Pug task ', 'green');
    return gulp
        .src('*.pug')
        .pipe(pug({pretty: true}))
        .pipe(gulp.dest('.'));
});

gulp.task('sass', () => {
    log('Running Sass task ', 'green');
    return gulp
        .src('./scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css-pre'));
});

gulp.task('postcss', ['sass'], () => {
    log('Running post-css task ', 'green');
    return gulp
        .src('./css-pre/*.css')
        .pipe(sourcemaps.init())
        .pipe(postcss([precss, prefixer]))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('css'));
});

gulp.task('vandana', [], () => {
    vandana.maa("green", "\t\t\t  May Maa Durga bless us !!\n\n");
});

gulp.task('default', [
    'vandana', 'ts', 'postcss', 'pug', 'browser-sync'
], () => {});

function log(data, color) {
    // logs to stdout , colorful if color is defined
    if (color) {
        console.log(chalk[color](data));
    } else {
        console.log(data);
    }
}